﻿using UnityEngine;
using System.Collections;

namespace Arcanoid.Test
{
    public struct PlayerDataStorage
    {
        public int Score;
        public int LevelScore;
    }
}