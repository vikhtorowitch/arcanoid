﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Arcanoid.Test
{
    public class RestartGameButtonEvents : MonoBehaviour, IPointerClickHandler
    {
        public GameController GameController;

        public void OnPointerClick(PointerEventData eventData)
        {
            GameController.Init();
        }
    }
}