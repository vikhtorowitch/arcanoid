﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Arcanoid.Test
{
    public class NextLevelButtonEvents : MonoBehaviour, IPointerClickHandler
    {
        public GameController GameController;

        public void OnPointerClick(PointerEventData eventData)
        {
            GameController.LoadNextLevel();
        }
    }
}