﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Arcanoid.Test
{
    public class PausePlayButtonEvents : MonoBehaviour, IPointerClickHandler
    {
        public GameController GameController;

        public void OnPointerClick(PointerEventData eventData)
        {
            GameController.SetPause(!GameController.IsPaused());
        }
    }
}