﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Arcanoid.Test
{
    public class RestartButtonEvents: MonoBehaviour, IPointerClickHandler
    {
        public GameController GameController;

        public void OnPointerClick(PointerEventData eventData)
        {
            GameController.ReloadCurrentLevel();
        }
    }
}