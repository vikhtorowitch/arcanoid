﻿using System.Collections;
using UnityEngine;

namespace Arcanoid.Test
{
    public abstract class BasicBooster: MonoBehaviour
    {
        public float BoostActiveTime;

        protected GameController _gameController;
        protected BoosterMainController _boosterMainController;

        public abstract void ActivateBoost();
        public abstract void DeactivateBoost();

        protected void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.CompareTag("Platform"))
            {
                _boosterMainController = gameObject.GetComponent<BoosterMainController>();

                if (_boosterMainController != null)
                {
                    _gameController = _boosterMainController.GameController;
                    _boosterMainController.Sprite.SetActive(false);
                    ActivateBoost();

                    StartCoroutine(BoostCooldown());
                }
            }
        }

        protected IEnumerator BoostCooldown()
        {
            yield return new WaitForSeconds(BoostActiveTime);

            DeactivateBoost();
            GameObject.Destroy(gameObject);
        }
    }
}
