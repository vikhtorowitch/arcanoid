﻿using UnityEngine;

namespace Arcanoid.Test
{
    public class PlatformScaleBooster: BasicBooster
    {
        public Vector3 NewPlatformScale;

        private Vector3 _oldPlatformScale;
        private Transform _platformTranfsorm;
        
        public override void ActivateBoost()
        {
            var levelController = _gameController.GetCurrentLevel();

            _platformTranfsorm = levelController.Platform.gameObject.transform;
            _oldPlatformScale = _platformTranfsorm.localScale;

            _platformTranfsorm.localScale = NewPlatformScale;
        }

        public override void DeactivateBoost()
        {
            _platformTranfsorm.localScale = _oldPlatformScale;
        }
    }
}
