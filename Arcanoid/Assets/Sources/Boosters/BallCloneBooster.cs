﻿using UnityEngine;

namespace Arcanoid.Test
{
    public class BallCloneBooster : BasicBooster
    {
        private LevelController _levelController;
        private GameObject _ballCloneInstance;

        public override void ActivateBoost()
        {
            _levelController = _gameController.GetCurrentLevel();
            _ballCloneInstance = Instantiate(_levelController.Balls[0].gameObject);

            _ballCloneInstance.transform.SetParent(_levelController.BallsRoot.transform);

            var ballController = _ballCloneInstance.GetComponent<BallController>();
            
            if (ballController != null)
            {
                ballController.Target = ballController.Target * (-1);
               _gameController.AddBall(ballController);
            }

            GameObject.Destroy(gameObject);
        }

        public override void DeactivateBoost()
        {
        }
    }
}
