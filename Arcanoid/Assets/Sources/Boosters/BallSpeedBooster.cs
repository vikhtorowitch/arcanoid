﻿using UnityEngine;

namespace Arcanoid.Test
{
    public class BallSpeedBooster: BasicBooster
    {
        public float NewBallSpeed;

        private float _oldBallSpeed;
        private LevelController _levelController;

        public override void ActivateBoost()
        {
            _levelController = _gameController.GetCurrentLevel();

            _oldBallSpeed = _levelController.Balls[0].Speed;

            foreach (var ballController in _levelController.Balls)
            {
                ballController.Speed = NewBallSpeed;
            }
        }

        public override void DeactivateBoost()
        {
            foreach (var ballController in _levelController.Balls)
            {
                ballController.Speed = _oldBallSpeed;
            }
        }
    }
}
