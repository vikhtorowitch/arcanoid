﻿using System;
using UnityEngine;

namespace Arcanoid.Test
{
    public class MovingMonoBehaviour : MonoBehaviour
    {
        public float Speed;
        public bool Active = true;

        public Vector3 Target = Vector3.up * 1000;

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void FixedUpdate()
        {
            if (gameObject.activeSelf && Active)
            {
                float delta = Speed * Time.fixedDeltaTime;
                transform.position = Vector3.MoveTowards(transform.position, Target, delta);
            }
        }
    }
}
