﻿using System;
using UnityEngine;

namespace Arcanoid.Test
{
    public class BallController: MovingMonoBehaviour
    {
        public event EventHandler OnBallDestroyed;

        private void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.CompareTag("BottomCollider"))
            {
                Hide();

                if (OnBallDestroyed != null)
                {
                    OnBallDestroyed(this, EventArgs.Empty);
                }

                return;
            }

            var collisionNormal = coll.contacts[0].normal;
            var oldTarget = Target;

            Target = Vector3.Reflect(oldTarget, collisionNormal);
        }
    }
}