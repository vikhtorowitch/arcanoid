﻿using System;
using UnityEngine;

namespace Arcanoid.Test
{
    public class BrickController: MonoBehaviour
    {
        public int InitialArmor;
        public int CurrentArmor;
        public int Points;

        public event EventHandler OnBrickDestroyed;

        public void Hit()
        {
            CurrentArmor--;

            if (CurrentArmor <= 0)
            {
                Hide();

                if (OnBrickDestroyed != null)
                {
                    OnBrickDestroyed(this, EventArgs.Empty);
                }
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.tag == "Ball")
            {
                Hit();
            }
        }
    }
}