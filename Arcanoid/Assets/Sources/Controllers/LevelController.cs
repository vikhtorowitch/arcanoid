﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Arcanoid.Test
{
    public class LevelController: MonoBehaviour
    {
        public string LevelName;

        public GameObject BricksRoot;
        public GameObject BallsRoot;
        public GameObject PlatformRoot;

        public List<BrickController> Bricks;
        public List<BallController> Balls;
        public PlatformController Platform;

        public void Init()
        {
            if (BricksRoot != null)
            {
                Bricks = BricksRoot.GetComponentsInChildren<BrickController>().ToList();

                foreach (var brickController in Bricks)
                {
                    brickController.CurrentArmor = brickController.InitialArmor;
                }
            }

            if (BallsRoot != null)
            {
                Balls = BallsRoot.GetComponentsInChildren<BallController>().ToList();
            }
        }
    }
}