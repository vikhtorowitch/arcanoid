﻿using UnityEngine;

namespace Arcanoid.Test
{
    public class PlatformController: MonoBehaviour
    {
        public bool Active = true;

        void OnCollisionEnter2D(Collision2D coll)
        {
            //Debug.Log("Platform collision");
        }

        private void OnMouseDrag()
        {
            if (Active)
            {
                var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                position.z = 0;
                position.y = gameObject.transform.position.y;

                gameObject.transform.position = position;
            }
        }
    }
}