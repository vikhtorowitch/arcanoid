﻿using UnityEngine;

namespace Arcanoid.Test
{
    public class BoosterMainController: MovingMonoBehaviour
    {
        public GameObject Sprite;
        public GameController GameController;

        private bool needToDestroy = true;

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.CompareTag("Platform"))
            {
                needToDestroy = false;
            }

            if (coll.gameObject.CompareTag("BottomCollider") && needToDestroy)
            {
                GameObject.Destroy(gameObject);
            }
        }

        private void Start()
        {
            Target = Vector3.down * 1000;
        }
    }
}