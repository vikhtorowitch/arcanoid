﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Arcanoid.Test
{
    public class GameController: MonoBehaviour
    {
        public PlayerDataStorage PlayerData;

        public GameObject[] LevelsQuue;

        public GameObject StartPanel;
        public GameObject LoosePanel;
        public GameObject LevelCompletePanel;
        public GameObject WinPanel;

        public Text CurrentLevelText;
        public Text PointsText;

        public GameObject[] Boosters;

        public float ChanceToMakeBoost = 0.25f;

        private GameObject _levelInstance;
        private int _currentLevel;
        private LevelController _currentLevelController;

        private bool _active;

        #region Public methods

        public void Init()
        {
            PlayerData.Score = 0;
            PlayerData.LevelScore = 0;

            _currentLevel = 0;
            LoadLevel(LevelsQuue[_currentLevel]);
        }

        public void LoadLevel(GameObject level)
        {
            DisablePanels();

            if (_levelInstance != null)
            {
                GameObject.Destroy(_levelInstance);
            }

            _levelInstance = GameObject.Instantiate(level);
            _levelInstance.transform.SetParent(gameObject.transform);

            _currentLevelController = _levelInstance.GetComponent<LevelController>();

            if (_currentLevelController != null)
            {
                CurrentLevelText.text = _currentLevelController.LevelName;
                PointsText.text = PlayerData.Score.ToString();

                _currentLevelController.Init();

                foreach (var ballController in _currentLevelController.Balls)
                {
                    ballController.OnBallDestroyed += OnBallDestroyed;
                }

                foreach (var brickController in _currentLevelController.Bricks)
                {
                    brickController.OnBrickDestroyed += OnBrickDestroyed;
                }
            }
        }

        public LevelController GetCurrentLevel()
        {
            return _currentLevelController;
        }

        public void ReloadCurrentLevel()
        {
            Destroy(_levelInstance);
            LoadLevel(LevelsQuue[_currentLevel]);
        }

        public void LoadNextLevel()
        {
            _currentLevel++;
            LoadLevel(LevelsQuue[_currentLevel]);
        }

        public bool IsPaused()
        {
            return _active;
        }

        public void SetPause(bool paused)
        {
            _active = !paused;

            foreach (var ballController in _currentLevelController.Balls)
            {
                ballController.Active = _active;
            }

            _currentLevelController.Platform.Active = _active;
        }

        public void AddBall(BallController ballController)
        {
            _currentLevelController.Balls.Add(ballController);
            ballController.OnBallDestroyed += OnBallDestroyed;
        }

        #endregion

        #region Private methods

        private void Start()
        {
            if (StartPanel != null)
            {
                StartPanel.SetActive(true);
            }
        }

        private bool IsActiveElementsInList<T>(IEnumerable<T> list) where T : MonoBehaviour
        {
            var activeElements = list.Where(p => p.gameObject.activeSelf);
            return activeElements.Any();
        }

        private void OnBallDestroyed(object sender, EventArgs args)
        {
            if (sender is BallController)
            {
                _currentLevelController.Balls.Remove((sender as BallController));
            }

            if (!IsActiveElementsInList(_currentLevelController.Balls))
            {
                LooseCondition();
            }
        }

        private void OnBrickDestroyed(object sender, EventArgs args)
        {
            if (sender is BrickController)
            {
                PlayerData.Score += (sender as BrickController).Points;
                PointsText.text = PlayerData.Score.ToString();

                MakeRandomBoosterOrNot((sender as BrickController).gameObject.transform.localPosition);
            }

            if (!IsActiveElementsInList(_currentLevelController.Bricks))
            {
                WinCondition();
            }
        }

        private void MakeRandomBoosterOrNot(Vector3 boostCoords)
        {
            var maxIndex = Boosters.Length;
            var rnd = UnityEngine.Random.value;

            if (rnd <= ChanceToMakeBoost)
            {
                rnd = UnityEngine.Random.value;
                
                int rndIndex = Mathf.FloorToInt(rnd / (1 / (float)maxIndex));
                
                var boosterGameObject = GameObject.Instantiate(Boosters[rndIndex]);
                boosterGameObject.transform.SetParent(_levelInstance.transform);
                boosterGameObject.transform.localPosition = boostCoords;

                var boosterMainController = boosterGameObject.GetComponent<BoosterMainController>();

                if (boosterMainController != null)
                {
                    boosterMainController.GameController = this;
                }
            }
        }

        private void DisablePanels()
        {
            if (StartPanel != null)
            {
                StartPanel.SetActive(false);
            }

            if (LoosePanel != null)
            {
                LoosePanel.SetActive(false);
            }

            if (LevelCompletePanel != null)
            {
                LevelCompletePanel.SetActive(false);
            }

            if (WinPanel != null)
            {
                WinPanel.SetActive(false);
            }
        }

        private void WinCondition()
        {
            SetPause(true);

            if (_currentLevel >= LevelsQuue.Length - 1)
            {
                if (WinPanel != null)
                {
                    WinPanel.SetActive(true);
                }
            }
            else
            {
                if (LevelCompletePanel != null)
                {
                    LevelCompletePanel.SetActive(true);
                }
            }
        }

        private void LooseCondition()
        {
            if (LoosePanel != null)
            {
                LoosePanel.SetActive(true);
            }
        }
    }

    #endregion
        
}